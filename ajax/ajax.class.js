/**
 * Usuario:  Erik Serrano <erik1951@gmail.com, erik1951@protonmail.com>
 * Versión:  0.0.0.3
 * Creación: 2015-05-28T13:26:21Z
 * Documentación: https://xhr.spec.whatwg.org/ https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
 * Descripción: Clase encargada de permitir hacer conexiones al servidor mediante el objeto XMLHttpRequest (AJAX)
 */
function Ajax(pHttpMethod, pUrl, pAsync, pResponseType) {
    'use strict';

    // Tipos de valores permitidos para los parámetros enviados por el usuario
    var httpMethodAccepted = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'OPTIONS'],
        responseTypeAccepted = ['arraybuffer', 'blob', 'document', 'json', 'text'],
        eventAccepted = ['loadstart', 'progress', 'error', 'abort', 'timeout', 'load', 'loadend'],
        codeStatusAccepted = [0, 1, 2, 3, 4];

    // Párametros del constructor
    var httpMethod = httpMethodAccepted.indexOf(pHttpMethod.toUpperCase()) === -1 ? 'GET' : pHttpMethod.toUpperCase(),
        url = pUrl,
        async = (pAsync === null || pAsync === undefined) ? true : pAsync,
        responseType = responseTypeAccepted.indexOf(pResponseType.toLowerCase()) === -1 ? '' : pResponseType.toLowerCase();

    // Variables privadas
    var request = null,
        lastError = null,
        requestHeaders = null,
        data = null,
        codeFunctions = {};

    /**
     * Método encargado de abrir la conexión con el servidor
     */
    function openRequest() {
        request.open(httpMethod, url, async);
    }

    /**
     * Método encargado de agregar los encabezados al request
     */
    function setRequestHeader() {
        if (requestHeaders !== null) {
            var requestHeadersKeys = Object.keys(requestHeaders);

            requestHeadersKeys.forEach(function (propt) {
                request.setRequestHeader(propt, requestHeaders[propt]);
            });
        }
    }

    /**
     * Función encargada de obtner en formato cadena (param=value)
     * los datos para enviar en el request
     * @return {String}
     */
    function getFormData() {
        var dataSend = '';
        if (data !== null) {
            var dataKeys = Object.keys(data);
            dataKeys.forEach(function (val) {
                dataSend += '&' + val + '=' + data[val];
            });
        }
        dataSend = dataSend.length > 0 ? dataSend.substring(1) : null;
        return dataSend;
    }

    /**
     * Método encargado de establecer la función (callback) para
     * el evento del proceso (evt) del request
     * @param {String} evt - Nombre del evento
     * @param {Function} func - Funcíon (callback) para manejar evento
     * @return {Object} Último error registrado
     */
    function setEventFunction(evt, func) {
        if (request !== null) {
            if (eventAccepted.indexOf(evt) !== -1) {
                if (typeof func === 'function') {
                    request.addEventListener(evt, func);
                } else {
                    lastError = { message: 'Error: El parámetro func no es una función.', date: new Date() };
                }
            } else {
                lastError = { message: 'Error: Evento ' + evt + ' no definido.', date: new Date() };
            }
        }
        return lastError;
    }

    /**
     * Método encargado de abortar la conexión
     * @return {Object} Último error registrado
     */
    function abort() {
        if (request !== null) {
            request.abort();
            return null;
        }
        return  lastError;
    }

    /**
     * Método encargado de agregar un encabezado al request
     * @param {String} name - Encabezado
     * @param {String} value - Valor del encabezado
     * @return {Object} Último error registrado
     */
    function setHeader (name, value) {
        if (request !== null) {
            if (requestHeaders === null) {
                requestHeaders = {};
            }
            requestHeaders[name] = value;
            return null;
        }
        return lastError;
    }

    /**
     * Método encargado de agregar de manera masiva encabezados al request
     * @param {Object} bulk - Objeto con los encabezados de la forma {name:value}
     * @return {Object} Último error registrado
     */
    function setBulkRequestHeader(bulk) {
        var bulkKeys = Object.keys(bulk),
            addHeaderResult = null;

        bulkKeys.forEach(function (val) {
            if (addHeaderResult === null) {
                addHeaderResult = setHeader(val, bulk[val]);
            }
        });

        return addHeaderResult;
    }

    /**
     * Método encargado de agregar un parámetro al request
     * @param {String} name - Nombre del parámetro
     * @param {String} value - Valor del parámetro
     * @return {Object} Último error registrado
     */
    function setData(name, value) {
        if (request !== null) {
            if (data === null) {
                data = {};
            }
            data[name] = value;
            return null;
        }
        return lastError;
    }

    /**
     * Método encargado de agregar de manera masiva parámetros al request de la forma {name:value}
     * @param {Object} bulk - Parámetros
     * @return {Object} Último error registrado
     */
    function setBulkData(bulk) {
        var bulkKeys = Object.keys(bulk),
            setDataResult = null;

        bulkKeys.forEach(function(val) {
            if (setDataResult === null) {
                setDataResult = setData(val, bulk[val]);
            }
        });

        return setDataResult;
    }

    /**
     * Método encargado de agregar la función (callback) que se ejcutara
     * para el estatus (code) del request
     * @param {Number} code - Número de estatus
     * @param {Function} func - Función (callback)
     * @return {Object} Último error registrado
     */
    function setCodeFunction(code, func) {
        if (request !== null) {
            if (codeStatusAccepted.indexOf(code) !== -1) {
                if (typeof func === 'function') {
                    codeFunctions[code] = func;
                } else {
                    lastError = { message: 'Error: función no definida.', date: new Date() };
                }
            } else {
                lastError = { message: 'Error: estatus ' + code + ' no definido.', date: new Date() };
            }
        }
        return lastError;
    }

    /**
     * Método encargado de manejar evento change para el cambio de estatus del request
     */
    function onReadyStateChange() {
        if (codeFunctions.hasOwnProperty(request.readyState)) {
            if (typeof codeFunctions[request.readyState] === 'function') {
                codeFunctions[request.readyState](request.status, request.responseText);
            }
        }
    }

    /**
     * Método encargado de iniciar el request
     * @return {Object} Último error registrado
     */
    function send() {
        if (request !== null) {
            openRequest();
            if (request.readyState !== request.LOADING && request.readyState !== request.DONE) {
                request.responseType = responseType;
            }
            setRequestHeader();
            request.onreadystatechange = onReadyStateChange;
            request.send(getFormData());
            return null;
        }
        return lastError;
    }

    /**
     * Método encargado de retornar el último error registrado
     * @return {Object}
     */
    this.getLastError = function () {
        return lastError;
    };

    /**
     * Método encargado de retornar la instancia del request
     * @return {XMLHttpRequest}
     */
    this.getXMLHttpRequest = function () {
        return request;
    };

    /**
     * Método encargado de agregar de manera masiva encabezados al request de la forma {name:value}
     * @param {object} bulk
     * @return {Object} Último error registrado
     */
    this.setBulkRequestHeader = function (bulk) {
        return setBulkRequestHeader(bulk);
    };

    /**
     * Método encargado de agregar un encabezado al request
     * @param {String} name - Encabezado
     * @param {String} value - Valor del encabezado
     * @return {Object} Último error registrado
     */
    this.setRequestHeader = function (name, value) {
        return setHeader(name, value);
    };

    /**
     * Método encargado de agregar de manera masiva parámetros al request de la forma {name:value}
     * @param {Object} bulk - Parámetros
     * @return {Object} Último error registrado
     */
    this.setBulkData = function (bulk) {
        return setBulkData(bulk);
    };

    /**
     * Método encargado de agregar un parámetro al request
     * @param {String} name - Nombre del parámetro
     * @param {String} value - Valor del parámetro
     * @return {Object} Último error registrado
     */
    this.setData = function (name, value) {
        return setData(name, value);
    };

    /**
     * Método encargado de agregar la función (callback) que se ejcutara
     * para el estatus (code) del request
     * @param {Number} code - Número de estatus
     * @param {Function} func - Función (callback)
     * @return {Object} Último error registrado
     */
    this.setCodeFunction = function (code, func) {
        return setCodeFunction(code, func);
    };

    /**
     * Método encargado de establecer la función (callback) para
     * el evento del proceso (evt) del request
     * @param {String} evt - Nombre del evento
     * @param {Function} func - Funcíon (callback) para manejar evento
     * @return {Object} Último error registrado
     */
    this.setEventFunction = function (evt, func) {
        return setEventFunction(evt, func);
    };

    /**
     * Método encargado de abortar la conexión
     * @return {Object} Último error registrado
     */
    this.abort = function() {
        return abort();
    };

    /**
     * Método encargado de iniciar el request
     * @return {Object} Último error registrado
     */
    this.send = function () {
        return send();
    };

    /**
     * Constructor de la clase. Se revisa si está definido el objeto
     * XMLHttpRequest (o MSXML2.XMLHTTP/Microsoft.XMLHTTP para Internet Explorer) en el navegador
     */
    (function () {
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                request = new window.ActiveXObject("MSXML2.XMLHTTP");
            }
            catch (e) {
                lastError = { message: e.message,  date: new Date() };
                try {
                    request = new window.ActiveXObject("Microsoft.XMLHTTP");
                    lastError = null;
                }
                catch (ex) {
                    lastError = { message: ex.message,  date: new Date() };
                }
            }
        } else {
            request = null;
            lastError = { message: 'Error: Objeto XMLHttpRequest no definido.',  date: new Date() };
        }
    }());
}

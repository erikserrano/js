/**
 * Usuario:  Erik Serrano <erik1951@gmail.com, erik1951@protonmail.com>
 * Versión:  0.0.0.3
 * Creación: 2015-05-28T13:26:21Z
 * Documentación: http://www.w3.org/TR/geolocation-API/ https://developer.mozilla.org/en-US/docs/Web/API/Geolocation
 * Descripción: Clase encargada de obtener la ubicación del cliente mediante la API de Geolocation del navegador
 */
function Geo() {
    'use strict';

    // Variables privadas
    var isGeolocationEnabled = true,
        lat = 0,
        long = 0,
        lastException = null,
        date = null;

    /**
     * Método encargado de manejar la función (callback) correcta al método window.geolocation
     * @param {Object} info - Información de la posición del usuario (Position)
     * @param {Function} author - Función (callback)
     */
    function success(info, onSuccess) {
        lat = info.coords.latitude;
        long = info.coords.longitude;
        date = new Date();

        if (onSuccess !== null && onSuccess !== undefined && typeof onSuccess === 'function') {
            onSuccess(info);
        }
    }

    /**
     * Método encargado de manejar la función (callback) incorrecta al método window.geolocation
     * @param {Object} error - Error (PositionError)
     * @param {Function} author - Función (callback)
     */
    function error(error, onError) {
        lastException = error;

        if (onError !== null && onError !== undefined && typeof onError === 'function') {
            onError(error);
        }
    }

    /**
     * Método encargado de obtener la información de ubicación del usuario
     * @param {Function} onSuccess - Función (callback) encargada de ejecutarse en caso de exito al obtener la información de la posición
     * @param {Function} onError - Función (callback) encargada de ejecutarse en caso de error al obtener la información de la posición
     * @param {Object} options - Propiedades (PositionObjects) para el método getCurrentPosition (navigator.geolocation)
     */
    function getCurrentPosition(onSuccess, onError, options) {
    	// Revisamos si el navegador cuenta con navigator.geolocation
        if (isGeolocationEnabled) {

            // Revisamos el parámetro options
            if (options === undefined || options === null || (typeof options !== 'object')) {
                options = {enableHighAccuracy: true, timeout: 30000, maximumAge: 0};
            }

            // Llamamos al API de Geolocation
            navigator.geolocation.getCurrentPosition(
                function (info) {
                    success(info, onSuccess);
                },
                function (err) {
                    error(err, onError);
                },
                options
            );
        } else {
            // El sistema no tiene habilidtado el API navigator.geolocation
            error(
                {
                    code: 4,
                    message: 'No existe el objeto navigator.geolocation.'
                },
                onError
            );
        }
    }

    /**
     * Método encargado de obtener la última latitud encontrada
     * @returns {Number} Latitud
     */
    this.getLatitude = function () {
        return lat;
    };

    /**
     * Método encargado de obtener la última longitud encontrada
     * @returns {Number} Longitud
     */
    this.getLongitude = function () {
        return long;
    };

    /**
     * Método encargado de obtener la última excepción encontrada
     * @returns {Object} Excepción ({code, message})
     */
    this.getLastException = function () {
        return lastException;
    };

    /**
     * Método encargado de obtener la hora y fecha del último llamado al método success
     * @returns {Object} Date
     */
    this.getDate = function () {
        return date;
    };

    /**
     * Método encargado de obtener la información de ubicación del usuario
     * @param {Function} onSuccess - Función (callback) encargada de ejecutarse en caso de exito al obtener la información de la posición
     * @param {Function} onError - Función (callback) encargada de ejecutarse en caso de error al obtener la información de la posición
     * @param {Object} options - Propiedades (PositionObjects) para el método getCurrentPosition (navigator.geolocation)
     */
    this.getCurrentPosition = function (onSuccess, onError, options) {
        getCurrentPosition(onSuccess, onError, options);
    };

    /**
     * Constructor de la clase.
     *
     * Se revisa que exista el objeto geolocation en el navegador (navigator)
     */
    (function () {
        // Revisamos si existe la API
        if (navigator.geolocation === undefined || navigator.geolocation === null) {
            // El navegador no tiene la API
            isGeolocationEnabled = false;
            lastException = {
                code: 4,
                message: 'No existe el objeto navigator.geolocation.'
            };
        }
    }());
}

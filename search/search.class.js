/**
 * Usuario: Erik Serrano <erik1951@gmail.com, erik1951@protonmail.com>
 * Versión: 0.0.0.3
 * Creación: 2015-01-22T11:04:21Z
 * Descripción: Clase encargada de buscar dentro de los valores de una tabla que coincidan con un valor buscado
 */
function Search(query, hideClass) {
    'use strict';

    // Variables privadas
    var rows = [],
        rows_length = 0;

    /**
     * Método encargado de eliminar los espacios en blanco encontrados a la izquierda de una cadena
     * @param {String} value - Cadena original
     * @return {String} nueva cadena
     */
    function leftTrim(value) {
        var ret = value.split(''),
            ret_length =  ret.length,
            i = 0;
        for (i; i < ret_length; i += 1) {
            if (ret[i] === ' ') {
                return leftTrim(value.substring(i + 1));
            }
            i = ret_length + 1;
        }
        return ret.join('');
    }

    /**
     * Método encargado de eliminar los espacios en blanco encontrados a la derecha de una cadena
     * @param {String} value - Cadena original
     * @return {String} nueva cadena
     */
    function rightTrim(value) {
        var ret = value.split(''),
            ret_length = ret.length - 1,
            i = ret_length;
        for (i; i >= 0; i -= 1) {
            if (ret[i] === ' ') {
                return rightTrim(value.substring(0, i));
            }
            i = -1;
        }
        return ret.join('');
    }

    /**
     * Método encargado de eliminar los espacios en blanco a la izquierda y derecha de una cadena
     * @param {String} value - Cadena original
     * @return {String} nueva cadena
     */
    function trim(value) {
        return leftTrim(rightTrim(value));
    }

    /**
     * Método encargado de obtener el texto que componen una columna
     * Si el elemento original contiene elementos en su interior se recorre uno a uno hasta encontrar el texto
     * @param {NodeElement[]} childrens - Nodos que componen un elemento del DOM
     * @return {String}
     */
    function getInnerText(childrens) {
        var childrens_length = childrens.length,
            k = 0;
        for (k; k < childrens_length; k += 1) {
            if (childrens[k].children.length === 0) {
                return trim(childrens[k].textContent);
            }
            return getInnerText(childrens[k].children);
        }
        return '';
    }

    /**
     * Método encargado de ocultar o mostrar un renglon de la tabla que coincida/no coincida con la búsqueda
     * @param {DOMElement} item
     * @param {Boolean} status - Indicamos
     */
    function hideShow(item, status) {
        item.setAttribute('data-row-visible', status);
        var classes = item.className.split(' ');
        if (status) {
            if (classes.indexOf(hideClass) !== -1) {
                delete classes[classes.indexOf(hideClass)];
            }
        } else {
            if (classes.indexOf(hideClass) === -1) {
                classes.push(hideClass);
            }
        }
        item.className = classes.join(' ');
    }

    /**
     * Método encargado de buscar en los renglones/columnas de una tabla los elementos.
     * Los elementos que no coincidan su renglon se ocultara.
     * que coincidan con la búsqueda
     * @param {String} pattern - Valor a buscar
     */
    function getItems(pattern) {
        var i = 0,
            columns = null,
            flag = null,
            j = null,
            columnsLength = 0;

        // Recorremos uno a uno los renglones de la tabla
        for (i; i < rows_length; i += 1) {
            columns = rows[i].children;
            columnsLength = columns.length;
            flag = false;
            j = 0;

            // Recorremos una a una las columnas del renglon
            for (j; j < columnsLength; j += 1) {
                // Si la columna contiene más elementos del DOM en su interior
                // los inspeccionamos hasta encontrar el texto que lo compone
                if (columns[j].children.length > 0) {
                    // Revisamos el texto del elemento
                    if (getInnerText(columns[j].children).toUpperCase().indexOf(pattern) !== -1) {
                        flag = true;
                        break;
                    }
                }
                // Revisamos el texto de la columna
                if (columns[j].textContent.toUpperCase().indexOf(pattern) !== -1) {
                    flag = true;
                    break;
                }
            }
            // Mostramos(flag:true) u ocultamos(flag:false) el renglon
            hideShow(rows[i], flag);
        }
    }

    /**
     * Método encargado de agregar los parámetros data-row-id, data-row-created y data-row-visible
     * para los renglones que componene la tabla
     * @param {DOMElement} row - Renglon de la tabla
     * @param {Object} objInfo - Información correspondiente al renglon
     */
    function setHTMLRowId(row, objInfo) {
        row.setAttribute('data-row-id', objInfo.id);
        row.setAttribute('data-row-created', objInfo.created);
        row.setAttribute('data-row-visible', objInfo.visible);
    }

    /**
     * Método encargado de retornar los renglones visibles/ocultos dentro de la tabla
     * @param {Boolean} visible
     * @return {DOMElement[]}
     */
    function getItemsByVisible(visible) {
        return document.querySelectorAll(query + '[data-row-visible="' + visible + '"]');
    }

    /**
     * Método encargado de iniciar la búsqueda en los elementos de una tabla
     * que coincidan con el valor (%pattern%) buscado
     * @param {String} pattern
     */
    this.findItem = function (pattern) {
        getItems(pattern.toUpperCase());
    };

    /**
     * Método encargado de retornar los renglones visibles dentro de la tabla
     * @param {Boolean} visible
     * @return {DOMElement[]}
     */
    this.getVisible = function () {
        return getItemsByVisible(true);
    };

    /**
     * Método encargado de retornar los renglones ocultos dentro de la tabla
     * @param {Boolean} visible
     * @return {DOMElement[]}
     */
    this.getInvisible = function () {
        return getItemsByVisible(false);
    };

    /**
     * Constructor de la clase
     * @param {String} query - Query de la ubicación de la tabla donde se va a realizar la búsqueda
     */
    (function (query) {
        var addElementRows = function (query) {
            var previus = document.querySelectorAll(query),
                lengthPrevius = previus.length,
                indexPrevius = 0;
            for (indexPrevius; indexPrevius < lengthPrevius; indexPrevius += 1) {
                rows.push(previus[indexPrevius]);
                setHTMLRowId(rows[rows.length - 1], { id: rows.length - 1, created: new Date(), visible: true });
            }
        };

        if (typeof query === 'object') {
            var index = null;
            for (index in query) {
                addElementRows(query[index]);
            }
        } else {
            addElementRows(query);
        }

        rows_length = rows.length;
    }(query));
}

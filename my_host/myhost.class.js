/**
 * Usuario: Erik Serrano <erik1951@gmail.com, erik1951@protonmail.com>
 * Versión: 0.0.0.1
 * Creación: 2014-10-03T15:54:21Z
 * Descripción: Clase mediante la cual se puede obttener información del HOST
 */
function MyHost(project) {
    'use strict';

    /**
     * Variable encargada de almacenar la ruta relativa del proyecto
     */
    this.project = project !== '/' ? project : '';

    /**
     * Método encargado de obtener el ruta del request solicitado (/path/to/resource)
     * @return {String}
     */
    function getPath() {
        return location.pathname;
    }

    /**
     * Método encargado de obtener el host del request
     * @return {String}
     */
    function getHost() {
        return (location.protocol + '//') + location.host;
    }

    /**
     * Método encargado de obtener la ruta absoluta del request (http://host/path/to/resource)
     * @return {String}
     */
    function getFullPath() {
        return getHost() + location.pathname;
    }

    /**
     * Método encargado de obtener la ruta absoluta del proyecto (http://host/path/to/resource/proyect)
     * @return {String}
     */
    function getURL() {
        return (location.protocol + '//') + location.host + (!!project ? '/' + project : '');
    }

    /**
     * Método encargado de obtener el ruta del request solicitado (/path/to/resource)
     * @return {String}
     */
    this.getPath = function () {
        return getPath();
    };

    /**
     * Método encargado de obtener la ruta absoluta del request (http://host/path/to/resource)
     * @return {String}
     */
    this.getFullPath = function () {
        return getFullPath();
    };

    /**
     * Método encargado de obtener la ruta absoluta del proyecto (http://host/path/to/resource/proyect)
     * @return {String}
     */
    this.getURL = function () {
        return getURL();
    };

    /**
     * Método encargado de obtener el host del request
     * @return {String}
     */
    this.getHost = function () {
        return getHost();
    };

}

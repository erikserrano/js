/**
 * Usuario: Erik Serrano <erik1951@gmail.com, erik1951@protonmail.com>
 * Versión: 0.1
 * Creación: 2015-05-28T13:26:21Z
 * Documentación: https://notifications.spec.whatwg.org/ https://developer.mozilla.org/en-US/docs/Web/API/notification
 * Descripción: Clase encargada de mostrar notificaciones en el navegador
 */
function Notify(title, options) {
    'use strict';

    // Variables privadas
    var noti = null,
        error = null;

    /**
     * Variable encargada de almacenar el título de la notificación
     */
    this.title = title;

    /**
     * Variable encargada de almacenar las opciones de la notificación
     */
    this.options = options;

    /**
     * Método encargado de generar un ID
     * @return {String}
     */
    function getId() {
        return ((new Date()).getTime() + ' ' + Math.random()).replace('.', '').replace(' ', '');
    }

    /**
     * Método encargado de generar y mostrar una notificación
     * @param {Number} timeOut
     * @return {String} Último error registrado
     */
    function show(timeOut) {
        // Revisamos que no hay error previo
        if (error === null) {
            // Revisamos si ya existia previamente la notificación
            if (noti === null) {
                // Creamos una nueva notificación
                noti = new window.Notification(title, options);
            } else {
                // Cerramos la notificación actual
                noti.close();
                // Creamos una nueva notificación
                noti = new window.Notification(title, options);
            }
            // Revisamos que el time out (timeOut) sea un valor correcto
            timeOut = (timeOut === undefined || timeOut === null) ? 0 : timeOut;
            if (timeOut > 0) {
                // Mostramos notificación
                var handleShow = function () {
                    var container = this;
                    window.setTimeout(function () {
                        container.close();
                    }, timeOut);
                };

                noti.addEventListener('show', handleShow, false);
            }
        }
        return error;
    }

    /**
     * Método encargado de retornar el último error registrado
     */
    this.getError = function () {
        return error;
    };

    /**
     * Método encargado de generar y mostrar una notificación
     * @param {Number} timeOut
     * @return {String} Último error registrado
     */
    this.show = function (timeOut) {
        return show(timeOut);
    };

    /**
     * Método encargado de retornar la instancia de la notificación
     * @return {window.Notification}
     */
    this.getNotification = function () {
        return noti;
    };

    /**
     * Método encargado de obtener el ID de la notificación
     * @return {String}
     */
    this.getTag = function () {
        return this.options.tag;
    };

    // Revisamos si está definido el tag; si no le asignamos uno
    if (this.options.tag === undefined) {
        this.options.tag = error === null ? getId() : null;
    }

    // Revisamos si está definido la dirección; si no le asignamos el default (auto)
    if (this.options.dir === undefined) {
        this.options.dir = 'auto';
    }

    /**
     * Constructor de la clase.
     *
     * Revisamos que exista el objeto Notification dentro del navegador (window)
     */
    (function () {
        if (window.Notification === undefined) {
            error = 'window.Notification no definido.';
        }
        else if (window.Notification.permission === 'denied') {
            error = 'window.Notification no permitido.';
        }
        else {
            window.Notification.requestPermission(function (permission) {
                if (!window.Notification.hasOwnProperty('permission')) {
                    window.Notification.permission = permission;
                }
                if (permission !== 'granted') {
                    error = 'window.Notification no permitido.';
                }
            });
        }
    }());

}
